import express from 'express';

const app = express();
const port = 3000;
app.get('/', (req, res) => {
  res.send('Hello World!8');
});
app.listen(port, err => {
  if (err) {
    return console.error(err);
  }
  return console.log(`Hello World!`);
});